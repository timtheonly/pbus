export default class Search {
  constructor(el) {
    this.el = el
    this.el.classList.add("four", "columns")
    var h4 = this.el.appendChild(window.document.createElement("h4"))
    h4.innerText = "Enter Stop Number"

    var button = this.el.appendChild(window.document.createElement("a"))
    button.classList.add("button-primary", "button", "search-button", "disabled")
    button.innerText = "Search"

    var input = this.el.appendChild(window.document.createElement("input"))
    input.classList.add("search-input")
    input.setAttribute("placeholder", "Stop Number")
    input.setAttribute("type", "number")
    input.setAttribute("min", "1")
    input.onkeyup = this.inputOnKeyUp(button, input)
  }

  inputOnKeyUp(button, input){
    return function(event){
      if(input.value) {
        button.setAttribute("href", "stop.html?id=" + input.value)
        button.classList.remove("disabled")
      } else {
        button.removeAttribute("href")
        button.classList.add("disabled")
      }
    }
  }
}
