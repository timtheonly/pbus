var xhr = require("xhr-promise")
const Search = require("./Search").default


export default class Stop {
  constructor(el) {
    this.el = el
    this.errorElement = this.el.querySelector('div.error')
    this.stopInfoElement = this.el.querySelector('div.stop-info')
    this.realtimeDataElement = this.el.querySelector('div.realtimeinfo')

    var urlParams = new URLSearchParams(window.location.search)
    this.stopNumber = urlParams.get("id")

    if(this.stopNumber){
      this.getRealtimeInfo()
      this.getStopInfo()
      this.timer = setInterval(this.getRealtimeInfo(), 60000)
    } else {
      var searchEL = this.el.appendChild(window.document.createElement('div'))
      var search = new Search(searchEL)
    }
  }

  getRealtimeInfo() {
    var xhrPromise = new xhr()
    xhrPromise.send({
      method: 'GET',
      url: 'https://prettybus-cache.herokuapp.com/v2/rtpi/stop/' + this.stopNumber + '/'
    }).then(function(result){
      if(result.status == 200){
        var tbl = this.realtimeDataElement.appendChild(window.document.createElement('table'))
        tbl.classList.add('u-full-width')

        var response = result.responseText
        response.forEach(function(bus){
          var tr = tbl.appendChild(window.document.createElement('tr'))

          var routeTd = tr.appendChild(window.document.createElement('td'))
          routeTd.innerText = bus.route

          var destinationTd = tr.appendChild(window.document.createElement('td'))
          destinationTd.innerText = bus.destination

          var dueTd = tr.appendChild(window.document.createElement('td'))
          dueTd.innerText = bus.due === 'due' ? bus.due : bus.due + ' mins'
        })

      } else {
        this.errorElement.innerText = result.responseText.message
      }
    }.bind(this))
  }

  getStopInfo() {
    var xhrPromise = new xhr()
    xhrPromise.send({
      method: 'GET',
      url: 'https://prettybus-cache.herokuapp.com/v2/stop/' + this.stopNumber + '/'
    }).then(function(result){
      if(result.status == 200){
        var response = result.responseText

        var ELstopNumber = this.stopInfoElement.appendChild(window.document.createElement('div'))
        ELstopNumber.innerText = response.StopNumber
        ELstopNumber.classList.add('two', 'columns')

        var ELstopName = this.stopInfoElement.appendChild(window.document.createElement('div'))
        ELstopName.innerText = response.Description
        ELstopName.classList.add('four', 'columns')
      }
    }.bind(this))
  }
}
