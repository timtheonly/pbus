export default class NavBar {
  constructor(el) {
    this.el = el
    this.hamburger = this.el.querySelector('div.hamburger')
    this.menu = this.el.querySelector('div.nav-menu ul')

    this.hamburger.onclick = this.hamburgerOnClick(this.menu)
  }

  hamburgerOnClick(menu){
    return function(event){
      menu.classList.toggle('hideMenu')
    }
  }
}
